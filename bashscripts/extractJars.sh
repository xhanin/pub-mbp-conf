#!/bin/bash

for filename in $(find . -name "*.jar")
do
  dir=${filename%.*}
  mkdir $dir
  cd $dir
  tar -xvf ../$filename
  cd ..
done
