#!/bin/bash

# Manually installing brew cask
brew install caskroom/cask/brew-cask

# Allowing to install alternate cask versions for packages (like beta/eap)
brew tap caskroom/versions
brew tap homebrew/dupes

brewPackages="
ack
git
gnupg
go
graphviz
groovy
httpie
imagemagick
jenv
jsawk
mackup
maven
node
openssl
python
python3
ruby
scons
svg2png
tree
unrar
wget
zsh
"

brewCaskPackages="
alfred
android-file-transfer
brackets
caffeine
deezer
evernote
firefox
flycut
google-chrome
google-drive
gpgtools
intellij-idea
iterm2
java
java6
java7
microsoft-office-2011
skitch
skype
slack
spectacle
sync
tunnelblick
vlc
"

# Removing starting carriage return
brewCaskPackages="${brewCaskPackages:1:${#brewCaskPackages}-1}"
brewPackages="${brewPackages:1:${#brewPackages}-1}"
  
#Deactivated parallel installation of brew packages since it fails
#IFS=' '
#echo $brewCaskPackages | parallel -j 10 'brew cask install {}'
#echo $brewPackages | parallel -j 10 'brew install {}'
brew cask install $brewCaskPackages
brew install $brewPackages

